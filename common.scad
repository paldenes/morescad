//
// Copyright (C) 2019 Pal Denes
//
// License: GPL 3 or later - see COPYING
//

// A rectangle rounded at the corners, centered at the origin.
//
// width, height: size
// r: radius of the rounded corners
// opentop: if true, only the bottom two corners will be rounded (leaving the top "open")
module rounded_rect(width, height, r, opentop=false) {
     if (!opentop) translate([width/2-r, height/2-r, 0]) circle(r=r);
     translate([-width/2+r, -height/2+r, 0]) circle(r=r);
     translate([width/2-r, -height/2+r, 0]) circle(r=r);
     if (!opentop) translate([-width/2+r, height/2-r, 0]) circle(r=r);
     square(size=[width-2*r, height], center=true);
     translate([-width/2, -height/2+r, 0]) square(size=[width, opentop ? height-r : height-2*r]);
}

// A tapered cylinder (both top and bottom ends can be made narrower (or wider).
//
// d: total cylinder diameter
// h: total cylinder height
// d2: diameter of the tapered top-bottom section
// h2: height of the tapered top-bottom section
module tapered_cylinder(d, h, d2, h2) {
     cylinder(d2 = d, h = h2, d1 = d2);
     translate([0,0,h2-0.001]) cylinder(d = d, h = h-h2*2+0.002);
     translate([0,0,h-h2]) cylinder(d1 = d, h = h2, d2 = d2);
}

// A ring with optionally tapered inside or outside.
// od: outer diameter
// id: inner diameter
// id2: inner diameter at the top
// od2: outer diameter at the top
// h: height
// fni: value of $fn for the inside
// fno: value of $fn for the outside
module ring(od = 5, id = 3, id2 = 0, od2 = 0, h = 2, fni = 50, fno = 50) {
     difference() {
          cylinder(d1 = od, d2 = od2 == 0 ? od : od2, h = h, $fn = fno);
          translate([0,0,-0.001]) cylinder(d1 = id, d2 = id2 == 0 ? id : id2, h = h+0.002, $fn = fni);
     }
}

// A half sphere
// r: radius
// d: diameter (if radius is not specified)
module halfsphere(r, d) {
     r = r == undef ? d/2 : r;
     difference() {
          sphere(r);
          translate([0,0,-r]) cylinder(r=r, h=r);
     }
}

// Calculates the distance to position small objects to the sides of a larger object, while maintaining a given pitch for the small objects.
// For example to place screw holes to the left and right of some object, with the holes aligned to a grid of a given pitch:
//
//      total_len
//     +---------+
//  x  |         |  x
//     +---------+
//
//  |   |   |   |   |
//  pitch
// 
// Returns the distance of the x's from the sides: it will be at least min_dist and the the distance between the x's will be a multiple of pitch.
//       
// total_len: the length of the larger object
// pitch: the pitch for the small objects
// min_dist: minimum distance
function center_align_with_pitch(total_len, pitch, min_dist) =
     let (d = (total_len + min_dist * 2) / pitch,
          total = (min_dist < 0 ? floor(d) : ceil(d)) * pitch)
          (total - total_len) / 2;
