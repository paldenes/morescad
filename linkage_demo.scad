//
// Copyright (C) 2019 Pal Denes
//
// License: GPL 3 or later - see COPYING
//

use <MCAD/layouts.scad>
use <./common.scad>
use <./linkage.scad>
use <./common_demo.scad>

$fn = 50;

translate([-40,-40,0]) list(12) {
     demo("link()") link();
     demo("link(link_length=10, link_width=3)") link(link_length=10, link_width=3);
     demo("link(hole_d=2, joint_width=5, joint_h=4)") link(hole_d=2, joint_width=5, joint_h=4);
     demo("link(link_width=7, link_length=10, link_h=4)") link(link_width=7, link_length=10, link_h=4);
     demo("link(link_width=10, link_length=6)") link(link_width=10, link_length=6);
     demo("link(link_length=10, link_h=1)") link(link_length=10, link_h=1);
     demo("link(joint_width=9, nut_hole_d=6.3, nut_hole_h=1)", "// M3 nut") link(joint_width=9, nut_hole_d=6.3, nut_hole_h=1);
     demo("link() translate([10,-1,0]) cube([2,2,1])") link() translate([10,-1,0]) cube([2,2,1]);
}

translate([40,-40,0]) list(12) {
     demo("link() translate([10,0,0]) cylinder(d=2, h=4)") link() translate([10,0,0]) cylinder(d=2, h=4);
     demo("link(joint_h=3, profile=true, conn_length=20, conn_transition=5)", "translate([0,1,0]) rounded_rect(3,2,1)", lx=10) link(joint_h=3, profile=true, conn_length=20, conn_transition=5) translate([0,1,0]) rounded_rect(3,2,1);
     demo("link(link_width=7, link_length=5, profile=true, conn_length=20, conn_transition=5, link_h=4)", "translate([0,1,0]) rounded_rect(3,2,1)", lx=10) link(link_width=7, link_length=5, profile=true, conn_length=20, conn_transition=5, link_h=4) translate([0,1,0]) rounded_rect(3,2,1);
     demo("link(collar_d=5, collar_h=1)") link(collar_d=5, collar_h=1);
     demo("link(collar_d=5, collar_h=-1)") link(collar_d=5, collar_h=-1);
     demo("link(hole_fn=8)") link(hole_fn=8);
     demo("link(hole_id=2, hole_id_h=1, collar_d=5, collar_h=1)") link(hole_id=2, hole_id_h=1, collar_d=5, collar_h=1);
}
