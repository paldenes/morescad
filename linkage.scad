//
// Copyright (C) 2019 Pal Denes
//
// License: GPL 3 or later - see COPYING
//

use <MCAD/layouts.scad>
use <./common.scad>;

// A link with a hinge joint.
//
// hole_d: joint hole diameter
// hole_id: optional internal diameter for the hole to make the lower section narrower
// hole_id_h: optional height of the lower section of the hole
// joint_h: height of the joint
// joint width: the width (diameter) of the joint
// link_h: height of the link (default is the same as joint_h)
// link_length: the length of the link (bar)
// double: doubles the thickness of the link
// nut_hole_d: diameter of an optional hole for a nut to be inserted in the joint
// nut_hole_h: height (depth) of the nut hole
// collar_d: diameter for an optional collar around the hole
// collar_h: height of the optional collar (can be negative)
// profile: if true, the child is used as a (2D) profile for an additional connector (bar), automatically rotated
// conn_length: (with profile): adds an additional connector with this length, using the profile
// conn_transition: the length of the section to transition the "joint bit" to the "connector" (using hull())
// hole_fn: number of fragments to use for the hole ($fn value)
module link(hole_d = 3.2, hole_id = 0, hole_id_h = 0, joint_h = 2, joint_width = 7, link_width = 4, link_h = undef, link_length = undef, double = false, nut_hole_d=0, nut_hole_h=0, collar_d=0, collar_h=0, profile=false, conn_length=0, conn_transition=0, hole_fn=0) {
     link_length = link_length == undef ? joint_width/2 : link_length;
     link_h = link_h == undef ? joint_h : link_h;
     taper_d_offs = 0.6;
     taper_h = 0.4;
     neg_collar_h = collar_h < 0 ? -collar_h : 0;
     collar_h = collar_h >= 0 ? collar_h : 0;

     module joint_profile(joint_h, width, length) {
          translate([length-0.01, -width/2, 0]) cube([0.01, width, link_h]);
     }

     difference() {
          union() {
               cylinder(d = joint_width, h = joint_h);
               if (link_width <= joint_width) {
                    translate([0, -link_width/2, 0]) cube([link_length, link_width, link_h]);
               } else {
                    w_offs = (link_width - joint_width)/2;
                    pts = [
                         [ 0,           0,                     0 ],  //0
                         [ link_length, -w_offs,               0 ],  //1
                         [ link_length, joint_width + w_offs,  0 ],  //2
                         [ 0,           joint_width,           0 ],  //3
                         [ 0,           0,                     link_h ],  //4
                         [ link_length, -w_offs,               link_h ],  //5
                         [ link_length, joint_width + w_offs,  link_h ],  //6
                         [ 0,           joint_width,           link_h ]]; //7
                    faces = [
                         [0,1,2,3],  // bottom
                         [4,5,1,0],  // front
                         [7,6,5,4],  // top
                         [5,6,2,1],  // right
                         [6,7,3,2],  // back
                         [7,4,0,3]]; // left
                    translate([0, -joint_width/2, 0]) polyhedron(pts, faces);
               }
               if (collar_d > 0 && collar_h > 0) {
                    translate([0,0,joint_h]) cylinder(d=collar_d, h=collar_h);
               }
          }
          if (hole_id > 0) {
               translate([0, 0, -0.01]) rotate([0,0,90]) tapered_cylinder(d = hole_id, h = hole_id_h*2, d2 = hole_id+taper_d_offs, h2 = taper_h);
               translate([0, 0, hole_id_h-0.01]) rotate([0,0,90]) cylinder(d = hole_d, h = joint_h+collar_h-hole_id_h+0.02, $fn=(hole_fn == 0 ? $fn : hole_fn));
               translate([0, 0, joint_h+collar_h-taper_h]) rotate([0,0,90]) tapered_cylinder(d = hole_d+taper_d_offs, h = joint_h+collar_h-hole_id_h+0.02, d2 = hole_d, h2 = taper_h, $fn=(hole_fn == 0 ? $fn : hole_fn));
          } else {
               translate([0, 0, hole_id_h-0.01]) rotate([0,0,90]) tapered_cylinder(d = hole_d, h = joint_h+collar_h-hole_id_h+0.02, d2 = hole_d+taper_d_offs, h2 = taper_h, $fn=(hole_fn == 0 ? $fn : hole_fn));
          }
          if (link_h > joint_h) {
               translate([0, 0, joint_h-0.001]) cylinder(d = joint_width+taper_d_offs, h = link_h);
               translate([-0.001, -link_width, joint_h]) cube([joint_width/3+0.001, link_width*2, link_h]);
          }
          if (nut_hole_d > 0) {
               translate([0,0,joint_h-nut_hole_h]) cylinder(d = nut_hole_d, h = nut_hole_h+0.1, $fn=6);
          }
          if (neg_collar_h > 0) {
               translate([0,0,joint_h-neg_collar_h]) cylinder(d = collar_d, h = neg_collar_h+0.001);
          }
     }
     if ($children > 0) {
          hull() {
               joint_profile(joint_h, link_width, link_length);
               if (profile) {
                    translate([link_length + conn_transition,0,0]) rotate([90,0,90]) linear_extrude(0.1) children();
               } else {
                    children();
               }
          }
          if (profile) {
               translate([link_length + conn_transition,0,0]) rotate([90,0,90]) linear_extrude(conn_length - conn_transition - link_length) children();
          }
     }
}
