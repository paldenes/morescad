//
// Copyright (C) 2019 Pal Denes
//
// License: GPL 3 or later - see COPYING
//

use <MCAD/layouts.scad>
use <./common.scad>

$fn = 50;

module demo(label, label2 = "", lx = 0) {
     children();
     translate([14+lx,0,0]) linear_extrude(0.1) scale(0.2) text(label);
     translate([14+lx+4,-3,0]) linear_extrude(0.1) scale(0.2) text(label2);
}

grid(60, 12, false, 10) {
     demo("rounded_rect(20, 10, 2)") rounded_rect(20, 10, 2);
     demo("rounded_rect(20, 10, 5, true)") rounded_rect(20, 10, 5, true);

     demo("tapered_cylinder(5, 10, 4, 1)") tapered_cylinder(5, 10, 4, 1);
     demo("tapered_cylinder(5, 10, 6, 3)") tapered_cylinder(5, 10, 6, 3);

     demo("ring()") ring();
     demo("ring(id=4, od=8, h=5)") ring(id=4, od=8, h=5);
     demo("ring(id=2, id2=5, od=6, h=5)") ring(id=2, id2=5, od=6, h=5);
     demo("ring(id=5, id2=2, od=6, od2=3, h=5)") ring(id=5, id2=2, od=6, od2=3, h=5);
}

