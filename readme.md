# MoreSCAD: yet another OpenSCAD library of useful things

## `common.scad`

Some common shapes and utilities.

![](common.png)

## `linkage.scad`

A module to generate various linkages with a hinge joint.

![](link1.png)

![](link2.png)
